FROM alpine:latest
MAINTAINER Oleksii Filonenko <brightone@protonmail.com>

# enable edge repos
RUN sed -i -e 's/v3\.4/edge/g' /etc/apk/repositories
# enable testing
RUN echo 'http://dl-cdn.alpinelinux.org/alpine/edge/testing' >> /etc/apk/repositories

RUN apk update \
    && apk add \
    texlive-full \
    texlive-xetex \
    make
